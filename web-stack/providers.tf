terraform {
  required_providers {
  linode = {
    source = "linode/linode"
    version = "1.25.2"
    }
  }

/*
  // https://adriano.fyi/post/2020-05-29-how-to-use-linode-object-storage-as-a-terraform-backend/

  backend "s3" {
    bucket = "terraform-web-stack-sas"
    key = "terraforms.tfstate"
    region = "us-southeast"
    endpoint = "us-southeast.linodeobjects.com"
    skip_credentials_validation = true
    access_key = var.s3_access_key
    secret_key = var.s3_secret_key
    }
*/
  }

provider "linode" {
  token = var.linode_token
}
