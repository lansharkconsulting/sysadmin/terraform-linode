# These are set from ENV Vars:
#   linode_token = ""  # from env var: TF_VAR_LINODE_TOKEN

public_key_name = "sas-laptop"
region = "us-southeast"
web_node_count = 3
web_instance_type = "g6-standard-4"  # 8GB/160GB/4cpu
web_svr_type = "web"
web_image = "linode/ubuntu20.04"
db_node_count = 1
db_instance_type = "g6-standard-6"   # 16GB/320GB/8cpu
db_image = "linode/ubuntu20.04"
db_svr_type = "db"
dev_instance_type = "g6-standard-6"  # 16GB/320GB/8cpu
dev_svr_type = "dev"
dev_image = "linode/ubuntu20.04"
