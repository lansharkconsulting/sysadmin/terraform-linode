# Terraform Module to Provision a full web stack with dev server, web server(s),
# database server, and load balancer.  Eventually will have firewall, DNS and VPC


# Generate a random password

resource "random_string" "password" {
  length = 32
  special = true
  upper = true
  lower = true
  number = true
}


# webserver configuration

module "webserver" {
  source = "../modules/webserver"
  public_key_name = var.public_key_name
  root_pass = "${random_string.password.result}"
  region = var.region
  web_node_count = var.web_node_count
  web_instance_type = var.web_instance_type
  web_svr_type = var.web_svr_type
  web_image = var.web_image
  client = var.client
}

# database configuration

module "dbserver" {
  source = "../modules/dbserver"
  public_key_name = var.public_key_name
  root_pass = "${random_string.password.result}"
  region = var.region
  db_node_count = var.db_node_count
  db_instance_type = var.db_instance_type
  db_svr_type = var.db_svr_type
  db_image = var.db_image
  client = var.client
}

# devserver configuration

module "devserver" {
  source = "../modules/webserver"
  public_key_name = var.public_key_name
  root_pass = "${random_string.password.result}"
  region = var.region
  web_instance_type = var.dev_instance_type
  web_svr_type = var.dev_svr_type
  web_image = var.dev_image
  client = var.client
}

# node balancer configuration

module "loadbalancer" {
  source = "../modules/nodebalancer"
  region = var.region
  node_count = var.web_node_count
  web_server_ips = module.webserver.web_servers_ips
  web_svr_type = var.web_svr_type
  client = var.client
}
