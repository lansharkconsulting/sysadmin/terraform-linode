output "node_balancer_ip_hostname" {
  value = module.loadbalancer.node_balancer_ip_hostname
}

output "node_balancer_ip" {
  value = module.loadbalancer.node_balancer_ip
}
