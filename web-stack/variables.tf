variable client {
  description = "Client Name/Abbreviation"
  nullable = false
  type = string
}
variable linode_token {
  description = "Linode API Token"
  nullable = false
  sensitive = true
  type = string
}
variable root_pass {
  description = "The root password to be configured on the nodes"
  nullable = false
  sensitive = true
  type = string
}
variable public_key_name {
  description = "SSH Public Key label on linode"
  default = "sas-laptop"
  nullable = false
  type = string
}
variable region {
  description = "The Linode Region in which the resource will be created"
  default = "us-southeast"
  type = string
}
variable web_node_count {
  description = "The number of web nodes to configure"
  default = 3
  type = number
}
variable web_instance_type {
  description = "The Linode Node Type of the web nodes"
  default = "g6-standard-4"
  type = string
}
variable web_svr_type {
  description = "The server type of the web nodes"
  default = "web"
  type = string
}
variable web_image {
  description = "The Linode image type of the web nodes"
  default = "linode/ubuntu20.04"
  type = string
}
variable db_node_count {
  description = "The number of db nodes to configure"
  default = 1
  type = number
}
variable db_instance_type {
  description = "The Linode Node Type of the db nodes"
  default = "g6-standard-6"
  type = string
}
variable db_svr_type {
  description = "The server type of the db nodes"
  default = "db"
  type = string
}
variable db_image {
  description = "The Linode image type of the db nodes"
  default = "linode/ubuntu20.04"
  type = string
}
variable dev_instance_type {
  description = "The Linode Node Type of the dev nodes"
  default = "g6-standard-6"
  type = string
}
variable dev_svr_type {
  description = "The server Type of the dev nodes"
  default = "dev"
  type = string
}
variable dev_image {
  description = "The Linode image type of the dev nodes"
  default = "linode/ubuntu20.04"
  type = string
}
