variable client {
  description = "Client Name/Abbreviation"
  nullable = false
  type = string
}
variable public_key_name {
  description = "The name of the SSH Key to be installed"
  default = "sas-laptop"
  nullable = false
  type = string
}
variable root_pass {
  description = "The root password set on the db server"
  nullable = false
  sensitive = true
  type = string
}
variable region {
  description = "The linode region in which the db server will be created"
  default = "us-southeast"
  type = string
}
variable db_node_count {
  description = "The number of db servers to configure"
  default = 1
  type = number
}
variable db_svr_type {
  description = "The server type to use for the db servers"
  default = "db"
  type = string
}
variable db_instance_type {
  description = "The linode node type to use for the db server"
  default = "g6-standard-6"
  type = string
}
variable db_image {
  description = "Image to use for Linode db server"
  default = "linode/ubuntu20.04"
  type = string
}
