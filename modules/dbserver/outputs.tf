output "db_servers_ips" {
  value = linode_instance.db.*.private_ip_address
}
