variable client {
  description = "Client Name/Abbreviation"
  nullable = false
  type = string
}
variable region {
  description = "The linode region in which the load balancer will be created"
  default = "us-southeast"
  type = string
}
variable node_count {
  description = "The number of web servers to configure for load balancing"
  default = 1
  type = number
}
variable web_server_ips {
  description = "The IP addresses of the created web servers"
  type = list(string)
}
variable web_svr_type {
  description = "The server type to use for the web servers"
  default = "web"
  type = string
}
