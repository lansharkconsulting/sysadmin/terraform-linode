terraform {
  required_providers {
    linode = {
      source = "linode/linode"
      version = "1.25.2"
    }
  }
}

resource "linode_nodebalancer" "lb" {
  label = "${var.client}-loadbalancer"
  region = var.region
  client_conn_throttle = 20
  tags = [ var.client ]
}

resource "linode_nodebalancer_config" "lb-config" {
  nodebalancer_id = linode_nodebalancer.lb.id
  port = 80
  protocol = "http"
  check = "http"
  check_path = "/"
  check_attempts = 3
  check_timeout = 3
  check_interval = 5
  stickiness = "http_cookie"
  algorithm = "roundrobin"
}

resource "linode_nodebalancer_node" "webnode" {
  count = var.node_count
  nodebalancer_id = linode_nodebalancer.lb.id
  config_id = linode_nodebalancer_config.lb-config.id
  address = "${element(var.web_server_ips, count.index)}:8080"
  label = "${var.client}-${var.web_svr_type}-server-${count.index + 1}"
  weight = 100
}
