terraform {
  required_providers {
    linode = {
      source = "linode/linode"
      version = "1.25.2"
    }
  }
}

data "linode_sshkey" "ssh-key" {
  label = var.public_key_name
}

resource "linode_instance" "web" {
  count = var.web_node_count
  label = "${var.client}-${var.web_svr_type}-server-${count.index + 1}"
  image = var.web_image
  region = var.region
  type = var.web_instance_type
  authorized_keys = [data.linode_sshkey.ssh-key.ssh_key]
  root_pass = var.root_pass
  backups_enabled = true
  watchdog_enabled = true

  group = "${var.client}"
  tags = [ var.client ]
  private_ip = true


  connection {
    type = "ssh"
    user = "root"
    password = var.root_pass
    host = self.ip_address
  }

  provisioner "file" {
    source = "setup_script.sh"
    destination = "/tmp/setup_script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/setup_script.sh",
      "/tmp/setup_script.sh",
    ]
  }
}
