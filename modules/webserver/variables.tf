variable client {
  description = "Client Name/Abbreviation"
  nullable = false
  type = string
}
variable public_key_name {
  description = "The name of the SSH Key to be installed"
  default = "sas-laptop"
  nullable = false
  type = string
}
variable root_pass {
  description = "The root password set on the web servers"
  nullable = false
  sensitive = true
  type = string
}
variable region {
  description = "The linode region in which the web servers will be created"
  default = "us-southeast"
  type = string
}
variable web_node_count {
  description = "The number of web servers to configure"
  default = 1
  type = number
}
variable web_svr_type {
  description = "The server type to use for the web servers"
  default = "web"
  type = string
}
variable web_instance_type {
  description = "The linode node type to use for the web servers"
  default = "g6-standard-4"
  type = string
}
variable web_image {
  description = "Image to use for Linode web servers"
  default = "linode/ubuntu20.04"
  type = string
}
